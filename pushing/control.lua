--[[
Copyright (c) 2018-2020 ZwerOxotnik <zweroxotnik@gmail.com>
Licensed under the MIT licence;
Author: ZwerOxotnik

You can write and receive any information on the links below.
Source: https://gitlab.com/ZwerOxotnik/pushing
Mod portal: https://mods.factorio.com/mod/pushing
Homepage: https://forums.factorio.com/viewtopic.php?f=190&t=64623

]]--

local module = {}
module.events = {}

-- (x < 0, y > 0) or (x > 0, y < 0)
local function case_1(cause, entity)
  if cause.position.x > entity.position.x then
    if cause.position.y < entity.position.y then
      if entity.orientation > 0.125 and entity.orientation < 0.625 then
        return  0.025
      else
        return -0.025
      end
    else
      if entity.orientation > 0.375 and entity.orientation < 0.875 then
        return  0.025
      else
        return -0.025
      end
    end
  else
    if cause.position.y < entity.position.y then
      if entity.orientation > 0.125 and entity.orientation < 0.625 then
        return -0.025
      else
        return  0.025
      end
    else
      if entity.orientation > 0.375 and entity.orientation < 0.875 then
        return -0.025
      else
        return  0.025
      end
    end
  end
end

-- (x < 0, y < 0) or (x > 0, y > 0)
local function case_2(cause, entity)
  if cause.position.x > entity.position.x then
    if cause.position.y < entity.position.y then
      if entity.orientation > 0.125 and entity.orientation < 0.625 then
        return  0.025
      else
        return -0.025
      end
    else
      if entity.orientation > 0.375 and entity.orientation < 0.875 then
        return  0.025
      else
        return -0.025
      end
    end
  else
    if cause.position.y > entity.position.y then
      if entity.orientation > 0.125 and entity.orientation < 0.625 then
        return -0.025
      else
        return  0.025
      end
    else
      if entity.orientation > 0.375 and entity.orientation < 0.875 then
        return -0.025
      else
        return  0.025
      end
    end
  end
end

local function get_force(cause, entity)
  if entity.position.x > 0 then
    if entity.position.y > 0 then
      return case_2(cause, entity)
    else
      return case_1(cause, entity)
    end
  else
    if entity.position.y > 0 then
      return case_1(cause, entity)
    else
      return case_2(cause, entity)
    end
  end
end

local function on_entity_damaged(event)
  local entity = event.entity
  if not (entity and entity.valid) then return end -- and entity.orientation
  local cause = event.cause
  if not (cause and cause.valid) then return end

  -- local damage = event.final_damage_amount
  -- if damage < 4 then return end
  -- if event.damage_type == 'fire' then return end

  entity.orientation = entity.orientation + get_force(cause, entity)
end

module.events[defines.events.on_entity_damaged] = on_entity_damaged

module.set_events_filters = function()
  local filters = {{filter = "vehicle", mode = "and"}, {filter = "final-damage-amount", comparison = ">", value = 4, mode = "and"}, {filter = "damage-type", type = "fire", invert = true, mode = "and"}}
  script.set_event_filter(defines.events.on_entity_damaged, filters)
end

return module
